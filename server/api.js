/* jshint node: true */
'use strict';
var Hapi = require('hapi');
var Good = require('good');
var AWS = require("aws-sdk");
var moment = require("moment");
var randomstring = require("randomstring");
var api = {};

api.init = function (config, socket) {

    //set up api server
    var server = new Hapi.Server();
    server.connection({ port: config.apiPort, routes: { cors: true }  });
    
    //set up db connection
    
    /*
    var dynamodb = new AWS.DynamoDB({
        region: "us-east-1",
        accessKeyId:config.db.accessKeyId,
        secretAccessKey:config.db.secretAccessKey
    });
    */
    
    //GET Home
    server.route({
        method: 'GET',
        path: '/getHouse',
        handler: function (request, reply) {
            reply({
                name: 'Steves House',
                address: '123 Something Lane',
                features: {
                    lights: {
                        floors: [
                            [
                                {
                                    name: 'Side Door Hallway'
                                },
                                {
                                    name: 'Downstairs Bathroom'
                                },
                                {
                                    name: 'Kitchen'
                                },
                                {
                                    name: 'Dining Room'
                                },
                                {
                                    name: 'Living Room'
                                },
                                {
                                    name: 'Front Door'
                                }
                            ],
                            [
                                {
                                    name: 'Stairs'
                                },
                                {
                                    name:'Office',
                                },
                                {
                                    name:'Master'
                                },
                                {
                                    name:'Guest Room'
                                },
                                {
                                    name:'Upstairs Bathroom'
                                }
                            ]
                        ]
                    },
                    doors: {

                    },
                    hvac: {

                    },
                    pets: {

                    }
                }
            })
        }
    });
    
    //PUT Positions
    server.route({
        method: 'PUT',
        path: '/position',
        handler: function (request, reply) {
            var position = null;

            try {
                if (request.payload) {
                    position = JSON.parse(request.payload);

                    position.receivedAt = moment().format();

                    if (position && position.lat && position.lon) {
                        var params = {
                            Item: {
                                position_id: {
                                    S: position.receivedAt
                                },
                                position_range_id: {
                                    S: randomstring.generate(8)
                                },
                                Lat: {
                                    N: position.lat.toString()
                                },
                                Lon: {
                                    N: position.lon.toString()
                                }
                            },
                            TableName: 'meeseeks_positions'
                        };
                        /*
                        Temp
                        dynamodb.putItem(params, function(err, data) {
                            if (err){
                                console.log(err, err.stack); // an error occurred
                                reply(err);
                            }
                            else{
                                socket.sendPosition(position);
                                reply(data);
                            }
                        });
                        */
                    }
                    else {
                        console.log('Your message has no lat lon');
                        reply('Your message has no lat lon');
                    }
                }
                else {
                    console.log('Theres something wrong with your message');
                    reply('Theres something wrong with your message');
                }
            }
            catch (e) {
                console.log('Problem inserting position' + e.toString());
                reply('Problem inserting position' + e.toString());
            }
        }
    });

    server.register({
        register: Good,
        options: {
            reporters: [{
                reporter: require('good-console'),
                events: {
                    response: '*',
                    log: '*'
                }
            }]
        }
    }, function (err) {
        if (err) {
            throw err; // something bad happened loading the plugin
        }

        server.start(function () {
            server.log('info', 'API running at: ' + config.server + ':' + config.apiPort);
        });
    });
};

module.exports = api;
