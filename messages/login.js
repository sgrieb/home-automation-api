/* jshint node: true */
var Joi = require('joi');

var schema = Joi.object().keys({
    type: Joi.string().allow('login').required(),
    name: Joi.string().required()
});

module.exports = schema;